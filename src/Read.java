
public class Read implements Runnable {

	Employee e;
	
	public Read(Employee e) {
		this.e = e;
		Thread t = new Thread(this);
		t.start();
	}
	
	@Override
	public void run() {
		for (int i = 0; i < 10; i++) {
			synchronized (e) {
				System.out.print(e.id + " ");
				System.out.print(e.name + " ");
				System.out.print(e.surname + " ");
			}
			
			System.out.println();
			try {
				Thread.sleep(10);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
	}

}
