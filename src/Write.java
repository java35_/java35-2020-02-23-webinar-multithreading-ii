
public class Write implements Runnable {

	Employee e;
	
	public Write(Employee e) {
		this.e = e;
		Thread t = new Thread(this);
		t.start();
	}
	
	@Override
	public void run() {
		for (int i = 1; i < 11; i++) {
			synchronized (e) {
				e.id = i;
				e.name = "name" + i;
				e.surname = "surname" + i;
			}
			
			try {
				Thread.sleep(10);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
	}
}