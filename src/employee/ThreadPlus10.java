package employee;

public class ThreadPlus10 implements Runnable {
	volatile static int j = 0;
	
	@Override
	public void run() {
//		for (int i = 0; i < 10000; i++) {
//			
//		}
	}
	
	static int getJ() {
		return j;
	}
	
	static void setJ(int j) {
		ThreadPlus10.j = j;
	}
	
	synchronized static void jPlus1() {
		j = j + 1;
	}
}