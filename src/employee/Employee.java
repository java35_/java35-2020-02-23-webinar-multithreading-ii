package employee;

public class Employee {
	int id;
	String name;
	String surname;
	Object mutex = new Object();
	
	static int i;
	
	
	
	public Employee(int id, String name, String surname) {
		this.id = id;
		this.name = name;
		this.surname = surname;
	}

	int getId() {
		synchronized (mutex) {
			return id;
		}
	}
	
	void setId(int id) {
		synchronized (mutex) {
			this.id = id;
		}
	}
	
	void sayHello() {
		System.out.println("Hello");
	}
	
//	synchronized static int getI() {
//		return i;
//	}
//	
//	synchronized static void setI(int i) {
//		Employee.i = i; 
//	}
}
